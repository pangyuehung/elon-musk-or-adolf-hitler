module Pages.Home_ exposing (Model, Msg, page)

import Html exposing (Html)
import Html.Attributes
    exposing
        ( checked
        , class
        , disabled
        , for
        , href
        , id
        , required
        , src
        , start
        , type_
        , value
        )
import Html.Events exposing (onClick, onMouseEnter, onMouseLeave, preventDefaultOn)
import Json.Decode as Json
import Key exposing (Answer(..), Quote, quoteKey)
import List exposing (drop, filter, head, indexedMap, length, map, map2)
import Page exposing (Page)
import Random
import Random.List
import View exposing (View)



-- PAGE


page : Page Model Msg
page =
    Page.element
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }



-- INIT


type alias Model =
    { answers : Maybe (List Answer)
    , selected : Maybe Answer
    , hovered : Maybe Answer
    , quotes : List Quote
    }


init : ( Model, Cmd Msg )
init =
    ( { answers = Nothing
      , selected = Nothing
      , hovered = Nothing
      , quotes = quoteKey
      }
    , shuffleQuotes quoteKey
    )



-- UPDATE


type Msg
    = Response (Maybe Answer)
    | Select (Maybe Answer)
    | Hover (Maybe Answer)
    | Shuffle (List Quote)
    | Noop


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Response response ->
            case completion model.answers model.quotes of
                None ->
                    ( { model | answers = Just [] }, Cmd.none )

                Ongoing ->
                    case model.answers of
                        Nothing ->
                            ( model, Cmd.none )

                        Just responses ->
                            case response of
                                Nothing ->
                                    ( model, Cmd.none )

                                Just answer ->
                                    ( { model
                                        | answers = Just (responses ++ [ answer ])
                                        , selected = Nothing
                                      }
                                    , Cmd.none
                                    )

                Complete ->
                    ( { model
                        | answers = Nothing
                        , hovered = Nothing
                        , selected = Nothing
                      }
                    , shuffleQuotes model.quotes
                    )

        Select response ->
            ( { model | selected = response }, Cmd.none )

        Hover response ->
            ( { model | hovered = response }, Cmd.none )

        Shuffle newQuotes ->
            ( { model | quotes = newQuotes }, Cmd.none )

        Noop ->
            ( model, Cmd.none )


shuffle : List Quote -> Random.Generator (List Quote)
shuffle originalQuotes =
    Random.List.shuffle originalQuotes


shuffleQuotes : List Quote -> Cmd Msg
shuffleQuotes originalQuotes =
    Random.generate Shuffle (shuffle originalQuotes)



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- VIEW


onClickPreventDefault : msg -> Html.Attribute msg
onClickPreventDefault msg =
    preventDefaultOn "click" (Json.map alwaysPreventDefault (Json.succeed msg))


alwaysPreventDefault : msg -> ( msg, Bool )
alwaysPreventDefault msg =
    ( msg, True )


view : Model -> View Msg
view model =
    { title = "Who said it best? Elon Musk or Adolf Hitler"
    , body =
        [ header model.answers model.quotes
        , Html.main_ [] [ main_ model ]
        , footer
        ]
    }


progress : Maybe (List Answer) -> Int
progress answers =
    case answers of
        Nothing ->
            0

        Just something ->
            length something + 1


header : Maybe (List Answer) -> List Quote -> Html msg
header answers quotes =
    Html.header []
        [ Html.h1 [] [ Html.text "Who said it best?" ]
        , Html.progress
            [ Html.Attributes.max
                (String.fromInt (length quotes + 1))
            , value (String.fromInt (progress answers))
            ]
            []
        , Html.h2 []
            [ case completion answers quotes of
                None ->
                    Html.text "Considering class stance"

                Complete ->
                    Html.text ("You scored: " ++ score answers quotes)

                Ongoing ->
                    Html.text "Choose who you think said the following:"
            ]
        ]


score : Maybe (List Answer) -> List Quote -> String
score answers quotes =
    case answers of
        Nothing ->
            "ERR"

        Just responses ->
            String.fromInt
                (length
                    (filter
                        (\check -> check == True)
                        (map2 correct responses quotes)
                    )
                )
                ++ "/"
                ++ String.fromInt (length quotes)


correct : Answer -> Quote -> Bool
correct answer quote =
    answer == quote.by


type Completion
    = None
    | Ongoing
    | Complete


completion : Maybe (List Answer) -> List Quote -> Completion
completion answers quotes =
    case progress answers of
        0 ->
            None

        step ->
            if step > length quotes then
                Complete

            else
                Ongoing


main_ : Model -> Html Msg
main_ model =
    let
        complete =
            completion model.answers model.quotes

        attrs =
            case complete of
                Ongoing ->
                    if model.selected == Nothing then
                        [ disabled True ]

                    else
                        [ onClickPreventDefault (Response model.selected) ]

                _ ->
                    [ onClickPreventDefault (Response Nothing) ]

        children =
            [ Html.strong []
                [ Html.text
                    (case complete of
                        None ->
                            "Start"

                        Ongoing ->
                            "Next"

                        Complete ->
                            "Start Over"
                    )
                ]
            ]
    in
    (if complete == Complete then
        Html.main_

     else
        Html.form
    )
        []
        [ Html.ol (startingNumber model.answers model.quotes) (sections model)
        , case complete of
            Complete ->
                Html.button
                    [ onClickPreventDefault
                        (Response model.selected)
                    ]
                    children

            _ ->
                Html.label
                    [ onClickPreventDefault
                        (if model.selected == Nothing && complete == Ongoing then
                            Noop

                         else
                            Response model.selected
                        )
                    ]
                    [ Html.button [ onClickPreventDefault Noop ]
                        (Html.input [] [] :: children)
                    ]
        ]


sections : Model -> List (Html Msg)
sections model =
    case model.answers of
        Nothing ->
            [ section model Nothing Nothing Nothing ]

        Just answers ->
            if length answers < length model.quotes then
                [ section
                    model
                    (head (drop (progress model.answers - 1) model.quotes))
                    Nothing
                    Nothing
                ]

            else
                indexedMap
                    (\index answer ->
                        section
                            model
                            (head (drop index model.quotes))
                            (Just answer)
                            (Just index)
                    )
                    answers


startingNumber : Maybe (List Answer) -> List Quote -> List (Html.Attribute msg)
startingNumber answers quotes =
    case answers of
        Nothing ->
            []

        Just something ->
            if progress answers > length quotes then
                []

            else
                [ start (progress answers) ]


section : Model -> Maybe Quote -> Maybe Answer -> Maybe Int -> Html Msg
section model quote response index =
    let
        complete =
            completion model.answers model.quotes
    in
    Html.li
        (if quote == Nothing then
            [ class "intro" ]

         else
            []
        )
        ([ case quote of
            Nothing ->
                Html.p []
                    [ Html.text
                        ("Let's see how well can you distinguish between "
                            ++ "these two purveyors of anti-marxist "
                            ++ "\"socialism\" by trying to guess which one "
                            ++ "said each of the following quotes."
                        )
                    ]

            Just q ->
                Html.p [] [ Html.q [] [ Html.text q.quote ] ]
         , Html.h3 []
            [ Html.text
                (case complete of
                    None ->
                        "Meet the contenders"

                    Ongoing ->
                        "Who do you think said the above?"

                    Complete ->
                        "You answered: "
                            ++ (case response of
                                    Nothing ->
                                        "ERROR"

                                    Just Musk ->
                                        "Elon Reeves Musk"

                                    Just Hitler ->
                                        "Adolf Hitler"
                               )
                )
            ]
         , (if complete == Ongoing then
                Html.fieldset

            else
                Html.div
           )
            [ class "options" ]
            [ option
                Musk
                (optionState model Musk quote index)
                complete
                (model.selected == Just Musk)
            , option Hitler
                (optionState model Hitler quote index)
                complete
                (model.selected == Just Hitler)
            ]
         ]
            ++ (case ( quote, response ) of
                    ( Nothing, _ ) ->
                        []

                    ( _, Nothing ) ->
                        []

                    ( Just q, Just r ) ->
                        if complete == Complete then
                            [ Html.h3 []
                                [ Html.text
                                    (if correct r q then
                                        "Correct"

                                     else
                                        "Wrong"
                                    )
                                ]
                            , Html.a
                                [ href q.source.url ]
                                [ Html.em
                                    []
                                    [ Html.text
                                        ("Source: "
                                            ++ q.source.citation
                                        )
                                    ]
                                ]
                            ]

                        else
                            []
               )
        )


optionState : Model -> Answer -> Maybe Quote -> Maybe Int -> AnswerState
optionState model choice quote index =
    let
        complete =
            completion model.answers model.quotes == Complete

        selected =
            case ( model.answers, index, complete ) of
                ( Just answers, Just i, True ) ->
                    head (drop i answers) == Just choice

                _ ->
                    model.selected == Just choice

        hovered =
            model.hovered == Just choice
    in
    case ( quote, complete, selected ) of
        ( Just q, True, True ) ->
            if choice == q.by then
                Correct

            else
                Incorrect

        ( Just q, True, False ) ->
            if choice == q.by then
                Open

            else
                Rejected

        ( Nothing, _, _ ) ->
            Open

        ( _, False, True ) ->
            Selected

        ( _, _, _ ) ->
            Open


type AnswerState
    = Correct
    | Incorrect
    | Rejected
    | Selected
    | Open


option : Answer -> AnswerState -> Completion -> Bool -> Html Msg
option answer state complete selected =
    (if complete == Ongoing then
        Html.label

     else
        Html.div
    )
        ([ onClickPreventDefault (Select (Just answer))
         , onMouseEnter (Hover (Just answer))
         , onMouseLeave (Hover Nothing)
         , class "option"
         ]
            ++ (if complete == Ongoing then
                    [ for
                        (case answer of
                            Musk ->
                                "musk"

                            Hitler ->
                                "hitler"
                        )
                    ]

                else
                    []
               )
        )
        ((if complete == Ongoing then
            [ Html.input
                [ type_ "radio"
                , id
                    (case answer of
                        Musk ->
                            "musk"

                        Hitler ->
                            "hitler"
                    )
                , checked selected
                ]
                []
            ]

          else
            []
         )
            ++ [ Html.img
                    [ src (portraitSrc answer)
                    , class
                        (case state of
                            Correct ->
                                "correct"

                            Incorrect ->
                                "incorrect"

                            Rejected ->
                                "rejected"

                            Selected ->
                                "selected"

                            Open ->
                                "considering"
                        )
                    ]
                    []
               , Html.h4 []
                    [ Html.text
                        (case answer of
                            Musk ->
                                "Elon Reeve Musk"

                            Hitler ->
                                "Adolf Hitler"
                        )
                    ]
               ]
        )


portraitSrc : Answer -> String
portraitSrc answer =
    case answer of
        Musk ->
            "/images/Elon Musk.jpg"

        Hitler ->
            "/images/Adolf Hitler.jpg"


footer : Html msg
footer =
    Html.footer []
        [ Html.text "Capitalists and \"national socialists\" fundamentally agree on opposing Marxism; maybe it deserves a closer look. "
        , Html.a [ href "https://write.as/xdzu6k311j4an1oe.md" ] [ Html.text "Learn more about actual socialism" ]
        , Html.text " to decide for yourself."
        ]
